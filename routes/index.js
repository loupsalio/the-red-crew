var express = require('express');
var passport = require('passport');
var router = express.Router();
var userController = require("../controllers/userController")
var articleModel = require('../models/articleModel.js');
var userModel = require('../models/userModel.js');

/* GET home page. */
router.get('/', function (req, res, next) {
  articleModel.find(function (err, results) {
    userModel.find(function (err, users_list) {
      res.render('index', { title: 'The RED Crew', user: req.user, list: results, users: users_list });
    });
  });
});

router.post('/upload', userController.upload)

router.route('/login').post(
  passport.authenticate('local', {
    failureRedirect: '/login'
  }),
  function (req, res) {
    if (req.user.name == "Loupsalio")
      res.redirect('/');
    else
      res.redirect('/profile');
  })
  .get(userController.login)

router.route('/profile').get(userController.profile)

router.get('/logout', function (req, res, next) {
  if (req.session) {
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});

module.exports = router;
