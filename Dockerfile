FROM node:latest

# Définir le répertoire de travail de l'applicaiton à /app
RUN mkdir -p /app/trc
WORKDIR /app/trc

# Copier le contenu du répertoire courant dans le conteneur à l'adresse /app
ADD . /app/trc

# Définir les variable d'environnement
ENV NAME trc

COPY package.json /app/trc/
RUN npm install --quiet

COPY . /app

# Rendre le port 3000 accessible à l'extérieur du conteneur
EXPOSE 3000

CMD [ "npm", "start" ]
