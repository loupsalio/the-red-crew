'use strict';

module.exports = function (http) {
    var io = require('socket.io')(http, { 'pingInterval': 200, 'pingTimeout': 500 });

    var users = []

    io.on('connection', function (socket) {
        var current_user = "";
        socket.on('connected', function (name) {
            current_user = name;
            if (!(users.find(function (element) { return element == name })))
                users.push(name)
        });

        socket.on('disconnect', function () {
            users = users.filter(function (element) {
                return element != current_user;
            })
        });

        socket.on('disconnection', function () {
            users = users.filter(function (element) {
                return element != current_user;
            })
        });
    });

    setInterval(function () {
        io.sockets.emit("users", users)
    }, 300)
}