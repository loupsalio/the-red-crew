var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
if (!process.env.MONGO_URI)
    process.env.MONGO_URI = 'mongodb://localhost/trc'
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true
});

var User = require('./models/userModel');


mongoose.set('useCreateIndex', true)

    var client = new User({
        name: "Loupsalio",
        password: "starwolf"
    });
    client.save(function (err, client) {
        if (err) return console.log(err);
        else
            console.log("New client - %s:%s", client.clientId, client.clientSecret);
});

setTimeout(function () {
    mongoose.disconnect();
}, 5000);
