var userModel = require('../models/userModel.js');
var articleModel = require('../models/articleModel.js');
var multer = require('multer');

/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */
module.exports = {


    /**
     * userController.upload()
     */
    upload: function (req, res) {
        var Storage = multer.diskStorage({
            destination: function (req, file, callback) {
                callback(null, "public/images");
            },
            filename: function (req, file, callback) {
                callback(
                    null,
                    file.fieldname + "_" + Date.now() + "_" + file.originalname
                );
            }
        });
        var upload = multer({
            storage: Storage
        }).array("Picture");
        upload(req, res, function (err) {
            if (err) {
                return res.status(401).json({
                    message: err
                });
            }
            if (req.files == undefined || req.files.length == 0)
                return res.status(401).json({
                    message: "File can't be empty"
                });
            console.log("File uploaded : " + req.files[0].filename);
            userModel.findOneAndUpdate(
                {
                    name: req.body.Name
                },
                {
                    picture: req.files[0].filename
                },
                {
                    new: true
                },
                function (err, User) {
                    if (err)
                        return res.status(401).json({
                            message: err
                        });
                    if (!User)
                        return res.status(401).json({
                            message: "User not found"
                        });
                    res.redirect("/profile");
                }
            );
        });
    },

    /**
     * userController.profile()
     */
    profile: function (req, res) {
        if (!req.user)
            return res.redirect('/login')
        articleModel.find({ autor: req.user.name }, function (err, results) {
            if (err)
                return res.status(500).json({ error: err });
            userModel.find(function (err, users_list) {
                if (err)
                    return res.status(500).json({ error: err });
                return res.render('user', { user: req.user, list: results, users: users_list });
            });
        })
    },

    /**
     * userController.login()
     */
    login: function (req, res) {
        if (req.user)
            return res.redirect('/profile')
        return res.render('login_page');
    },

    /**
     * userController.admin()
     */
    admin: function (req, res) {
        if (!req.user || req.user.name != "Loupsalio")
            return res.redirect('/');
        userModel.find(function (err, users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting users.',
                    error: err
                });
            }
            return res.render('users', { list: users });
        });
    },

    /**
     * userController.htmlList()
     */
    htmlList: function (req, res) {
        userModel.find(function (err, users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting users.',
                    error: err
                });
            }
            return res.render('list_users', { list: users });
        });
    },

    /**
     * userController.list()
     */
    list: function (req, res) {
        userModel.find(function (err, users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            return res.json(users);
        });
    },

    /**
     * userController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        userModel.findOne({ _id: id }, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user.',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No such user'
                });
            }
            return res.json(user);
        });
    },

    /**
     * userController.create()
     */
    create: function (req, res) {
        console.log(req.body.name)
        console.log(req.body.password)
        var user = new userModel({
            name: req.body.name,
            password: req.body.password

        });

        user.save(function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating user',
                    error: err
                });
            }
            return res.status(201).json(user);
        });
    },

    /**
     * userController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        userModel.findOne({ _id: id }, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting user',
                    error: err
                });
            }
            if (!user) {
                return res.status(404).json({
                    message: 'No such user'
                });
            }

            user.name = req.body.name ? req.body.name : user.name;
            user.password = req.body.password ? req.body.password : user.password;

            user.save(function (err, user) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating user.',
                        error: err
                    });
                }

                return res.json(user);
            });
        });
    },

    /**
     * userController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        userModel.findByIdAndRemove(id, function (err, user) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the user.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
