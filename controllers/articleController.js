var articleModel = require('../models/articleModel.js');

/**
 * articleController.js
 *
 * @description :: Server-side logic for managing articles.
 */
module.exports = {

    /**
     * articleController.admin()
     */
    admin: function (req, res) {
        if (!req.user || req.user.name != "Loupsalio")
            return res.redirect('/');
        articleModel.find(function (err, articles) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting articles.',
                    error: err
                });
            }
            return res.render('articles', { list: articles });
        });
    },

    /**
     * userController.htmlList()
     */
    htmlList: function (req, res) {
        articleModel.find(function (err, articles) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting articles.',
                    error: err
                });
            }
            return res.render('list_articles', { list: articles });
        });
    },

    /**
     * articleController.list()
     */
    list: function (req, res) {
        articleModel.find(function (err, articles) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting article.',
                    error: err
                });
            }
            return res.json(articles);
        });
    },

    /**
     * articleController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        articleModel.findOne({ _id: id }, function (err, article) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting article.',
                    error: err
                });
            }
            if (!article) {
                return res.status(404).json({
                    message: 'No such article'
                });
            }
            return res.json(article);
        });
    },

    /**
     * articleController.create()
     */
    create: function (req, res) {
        var article = new articleModel({
            title: req.body.title,
            content: req.body.content,
            autor: req.body.autor

        });

        article.save(function (err, article) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating article',
                    error: err
                });
            }
            return res.status(201).json(article);
        });
    },

    /**
     * articleController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        articleModel.findOne({ _id: id }, function (err, article) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting article',
                    error: err
                });
            }
            if (!article) {
                return res.status(404).json({
                    message: 'No such article'
                });
            }

            article.title = req.body.title ? req.body.title : article.title;
            article.content = req.body.content ? req.body.content : article.content;
            article.autor = req.body.autor ? req.body.autor : article.autor;

            article.save(function (err, article) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating article.',
                        error: err
                    });
                }

                return res.json(article);
            });
        });
    },

    /**
     * articleController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        articleModel.findByIdAndRemove(id, function (err, article) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the article.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
