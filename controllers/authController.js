'use strict';

module.exports = function (app) {

    const passport = require('passport');
    const LocalStrategy = require('passport-local').Strategy;
    var mongoose = require('mongoose'),
        crypto = require('crypto'),
        user = require('../models/userModel.js');

    app.use(passport.initialize());
    app.use(passport.session());

    passport.serializeUser(function (user, cb) {
        cb(null, user.id);
    });

    passport.deserializeUser(function (id, cb) {
        user.findById(id, function (err, user) {
            cb(err, user);
        });
    });

    passport.use(new LocalStrategy(
        function (username, password, done) {
            user.findOne({
                name: username
            }, function (err, user) {
                if (err) {
                    return done(err);
                }

                if (!user) {
                    return done(null, false, {
                        message: 'Pas d\'utilisateur avec ce login.'
                    });
                }

                if (!user.checkPassword(password)) {
                    return done(null, false, {
                        message: 'Mauvais mot de passe.'
                    });
                }
                return done(null, user);
            });
        }
    ));
}
