var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypto = require("crypto");

var userSchema = new Schema({
	'name': { type: String, required: true },
	'picture': { type: String, default: "base.png" },
	'hashedPassword': { type: String, required: true },
	'salt': { type: String, required: true }
});

userSchema.methods.encryptPassword = function (password) {
	return crypto
		.createHmac("sha1", this.salt)
		.update(password)
		.digest("hex");
};

userSchema.virtual("userId").get(function () {
	return this.id;
});

userSchema.virtual("password")
	.set(function (password) {
		this._plainPassword = password;
		this.salt = crypto.randomBytes(32).toString("hex");
		this.hashedPassword = this.encryptPassword(password);
	})
	.get(function () {
		return this._plainPassword;
	});

userSchema.methods.checkPassword = function (password) {
	return this.encryptPassword(password) === this.hashedPassword;
};

module.exports = mongoose.model('user', userSchema);
