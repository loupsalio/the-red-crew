var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var articleSchema = new Schema({
	'title': { type: String, required: true },
	'content': { type: String, required: true },
	'autor': { type: String, required: true }
});

module.exports = mongoose.model('article', articleSchema);
