// Socketio process

var socket = io();

socket.on('users', function (list) {
    $(".fa-circle").each(function (i) {
        var element = $(this)
        var value = list.find(function (elem) {
            return elem == element.parent().children('span').text()
        })
        if (value)
            element.removeClass("text-grey").addClass("text-green");
        else
            element.removeClass("text-green").addClass("text-grey");
    })
    if ($(".connect_name").length) {
        socket.emit('connected', $(".connect_name").text())
    }
    else {
        socket.emit('disconnection')
    }
})

// Other processes

function alert(message) {
    $('#alert').text(message).fadeIn()
    setTimeout(() => {
        $('#alert').fadeOut()
    }, 3000)
}

function refresh() {
    request = $.ajax({
        url: "/users/list",
        type: "GET"
    });

    request.done(function (response, textStatus, jqXHR) {
        $("#form-list-user-body").html(response)
        console.log(textStatus)
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
        console.log(
            "The following error occurred: " +
            textStatus, errorThrown
        );
        alert(errorThrown);
    });
}

function refresh_articles() {
    request = $.ajax({
        url: "/articles/list",
        type: "GET"
    });

    request.done(function (response, textStatus, jqXHR) {
        $("#form-list-article-body").html('')
        $("#form-list-article-body").append(response)
        console.log(textStatus)
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
        console.log(
            "The following error occurred: " +
            textStatus, errorThrown
        );
        alert(errorThrown);
    });
}

var request;

$(document).ready(
    () => {

        // User admin //

        $('#form-edit-user').submit(function (event) {
            event.preventDefault();

            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            var name = $('#user-name').val();
            var password = $('#user-password').val();

            // Serialize the data in the form
            var formData = {
                'name': name,
                'password': password
            };

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/users",
                type: "POST",
                data: formData
            });


            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                // Log a message to the console
                console.log("Hooray, it worked!");
                console.log(textStatus)
                $('#user-name').val("");
                $('#user-password').val("");
                refresh()
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.log(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
                alert(errorThrown);
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });
        })

        $('body').on("click", ".user-delete", function (event) {
            event.preventDefault();

            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            var id = $(this).parent().children(".id-user").text();

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/users/" + id,
                type: "delete"
            });


            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                // Log a message to the console
                console.log("Hooray, it worked!");
                console.log(textStatus)
                refresh()
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.log(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
                alert(errorThrown);
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });
        })

        $('body').on("click", ".user-view", function () {
            var name = $(this).parent().parent().children(".name").text()
            var image = $(this).parent().parent().children(".image").html()
            $('.view-name').text(name)
            $('.view-image').html(image)
        })

        // article admin

        if ($('#article-content').length) {
            $('#article-content').richText({

                // text formatting
                bold: true,
                italic: true,
                underline: true,

                // text alignment
                leftAlign: true,
                centerAlign: true,
                rightAlign: true,
                justify: true,

                // lists
                ol: false,
                ul: false,

                // title
                heading: false,

                // fonts
                fonts: true,
                fontList: [
                    "Arial",
                    "Arial Black",
                    "Comic Sans MS",
                    "Courier New",
                    "Geneva",
                    "Georgia",
                    "Helvetica",
                    "Impact",
                    "Lucida Console",
                    "Tahoma",
                    "Times New Roman",
                    "Verdana"
                ],
                fontColor: true,
                fontSize: true,

                // uploads
                imageUpload: true,
                fileUpload: false,

                // media
                videoEmbed: false,

                // link
                urls: true,

                // tables
                table: false,

                // code
                removeStyles: true,
                code: true,

                // colors
                colors: [],

                // dropdowns
                fileHTML: '',
                imageHTML: '',

                // translations
                translations: {
                    'title': 'Title',
                    'white': 'White',
                    'black': 'Black',
                    'brown': 'Brown',
                    'beige': 'Beige',
                    'darkBlue': 'Dark Blue',
                    'blue': 'Blue',
                    'lightBlue': 'Light Blue',
                    'darkRed': 'Dark Red',
                    'red': 'Red',
                    'darkGreen': 'Dark Green',
                    'green': 'Green',
                    'purple': 'Purple',
                    'darkTurquois': 'Dark Turquois',
                    'turquois': 'Turquois',
                    'darkOrange': 'Dark Orange',
                    'orange': 'Orange',
                    'yellow': 'Yellow',
                    'imageURL': 'Image URL',
                    'fileURL': 'File URL',
                    'linkText': 'Link text',
                    'url': 'URL',
                    'size': 'Size',
                    'responsive': 'Responsive',
                    'text': 'Text',
                    'openIn': 'Open in',
                    'sameTab': 'Same tab',
                    'newTab': 'New tab',
                    'align': 'Align',
                    'left': 'Left',
                    'center': 'Center',
                    'right': 'Right',
                    'rows': 'Rows',
                    'columns': 'Columns',
                    'add': 'Add',
                    'pleaseEnterURL': 'Please enter an URL',
                    'videoURLnotSupported': 'Video URL not supported',
                    'pleaseSelectImage': 'Please select an image',
                    'pleaseSelectFile': 'Please select a file',
                    'bold': 'Bold',
                    'italic': 'Italic',
                    'underline': 'Underline',
                    'alignLeft': 'Align left',
                    'alignCenter': 'Align centered',
                    'alignRight': 'Align right',
                    'addOrderedList': 'Add ordered list',
                    'addUnorderedList': 'Add unordered list',
                    'addHeading': 'Add Heading/title',
                    'addFont': 'Add font',
                    'addFontColor': 'Add font color',
                    'addFontSize': 'Add font size',
                    'addImage': 'Add image',
                    'addVideo': 'Add video',
                    'addFile': 'Add file',
                    'addURL': 'Add URL',
                    'addTable': 'Add table',
                    'removeStyles': 'Remove styles',
                    'code': 'Show HTML code',
                    'undo': 'Undo',
                    'redo': 'Redo',
                    'close': 'Close'
                },

                // privacy
                youtubeCookies: false,

                // developer settings
                useSingleQuotes: false,
                height: 0,
                heightPercentage: 0,
                id: "",
                class: "",
                useParagraph: false
            });
            $(".richText-help").remove();
        }

        $('#form-edit-article').submit(function (event) {
            event.preventDefault();

            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            var title = $('#article-title').val();
            var content = $('#article-content').val();
            var autor = $('#article-autor').val();

            // Serialize the data in the form
            var formData = {
                'title': title,
                'content': content,
                'autor': autor
            };

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/articles",
                type: "POST",
                data: formData
            });


            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                // Log a message to the console
                console.log("Hooray, it worked!");
                console.log(textStatus)
                $('#article-title').val("");
                $('.richText-editor').html("");
                $('#article-autor').val("");
                refresh_articles()
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.log(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
                alert(errorThrown);
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });
        })

        $('#form-edit-article-user').submit(function (event) {
            event.preventDefault();

            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            var title = $('#article-title').val();
            var content = $('#article-content').val();
            var autor = $('#article-autor').val();

            // Serialize the data in the form
            var formData = {
                'title': title,
                'content': content,
                'autor': autor
            };

            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/articles",
                type: "POST",
                data: formData
            });


            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                // Log a message to the console
                console.log("LA BANANA");
                console.log(textStatus)
                location.reload();
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.log(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
                alert(errorThrown);
            });

            request.always(function () {
            });
        })

        $('body').on("click", ".article-delete", function (event) {
            event.preventDefault();

            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            var id = $(this).parent().children(".id-article").text();
            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/articles/" + id,
                type: "delete"
            });


            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                // Log a message to the console
                console.log("Hooray, it worked!");
                console.log(textStatus)
                refresh_articles()
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.log(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
                alert(errorThrown);
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });
        })

        $('body').on("click", ".article-delete-user", function (event) {
            event.preventDefault();

            if (request) {
                request.abort();
            }
            if (!confirm("Supprimer cet element?")) {
                return;
            }

            // setup some local variables
            var $form = $(this);

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            var id = $(this).parent().children(".id-article").text();
            // Let's disable the inputs for the duration of the Ajax request.
            // Note: we disable elements AFTER the form data has been serialized.
            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "/articles/" + id,
                type: "delete"
            });


            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                // Log a message to the console
                console.log("Hooray, it worked!");
                console.log(textStatus)
                location.reload();
            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.log(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
                alert(errorThrown);
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
            });
        })

        $('body').on("click", ".article-view", function () {
            var title = $(this).parent().parent().children(".title").text()
            var content = $(this).parent().parent().children(".content").text()
            var autor = $(this).parent().parent().children(".autor").text()
            $('.view-title').text(title)
            $('.view-content').html('');
            $('.view-content').append(content);
            $('.view-autor').text(autor)
        })

        // setInterval(connected, 300);

    }
)